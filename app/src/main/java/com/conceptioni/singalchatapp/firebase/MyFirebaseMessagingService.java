package com.conceptioni.singalchatapp.firebase;


import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.conceptioni.singalchatapp.R;
import com.conceptioni.singalchatapp.activity.MainActivity;
import com.conceptioni.singalchatapp.utils.Constant;
import com.conceptioni.singalchatapp.utils.SharedPrefs;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    String to_id = "",emoji = "",sound = "",To_Id = "",from_id,name = "",from_name = "",To_Name = "",From_Name = "";

    @SuppressLint("NewApi")
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.d("++++++data","+++++"+remoteMessage.getData() + "+++");

        if (remoteMessage.getData() != null){
            try {
                JSONObject object = new JSONObject(remoteMessage.getData());
                to_id = object.getString("to_id");
                emoji = object.getString("emoji");
                sound = object.getString("sound");
                from_id = object.getString("from_id");
                name = object.getString("to_name");
                from_name = object.getString("from_name");
            } catch (JSONException e) {
                e.printStackTrace();
            }
           sendnotification(to_id,emoji,sound,from_id,name,from_name);
        }

//        if (SharedPrefs.getSharedPref().getString(SharedPrefs.userdetail.user_id,"N/A").equalsIgnoreCase(from_id)){
//            To_Id = to_id;
//            From_Name = from_name;
//            To_Name = name;
//        }else {
//            To_Id = from_id;
//            From_Name = name;
//            To_Name = from_name;
//        }

        if (Constant.active){

//            if (SharedPrefs.getSharedPref().getString(SharedPrefs.userdetail.user_id,"N/A").equalsIgnoreCase(from_id)){
//                To_Id = to_id;
//            }else {
//                To_Id = from_id;
//            }
            updateChatScreen();
        }

//        if (remoteMessage.getData() != null){
//            Log.d("++++++data","+++++"+remoteMessage.getData() + "+++");
//            try {
//                JSONObject object = new JSONObject(remoteMessage.getData());
//                String text = object.getString("text");
//                Log.d("++++++data","+++++"+remoteMessage.getData() + "+++"+text);
//                updateChatScreen(text);
//            } catch (JSONException e) {
//                e.printStackTrace();
//                Log.d("++++++dataerror","+++++nodata");
//            }
//        }else {
//            Log.d("++++++data","+++++nodata");
//        }
    }

    private void updateChatScreen() {
        Intent intent = new Intent("broadcast_chat_message");
        this.sendBroadcast(intent);
    }

//    @TargetApi(Build.VERSION_CODES.O)
//    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
//    public void performheadsUP() {
//
//        Log.d("+++++id","++++"+To_Name + "++++"+From_Name + "++++"+To_Id);
//
//        //RemoteViews remoteViews = new RemoteViews(getPackageName(),R.layout.rowitem_nav);
//        String CHANNEL_ID = "my_channel_01";// The id of the channel.
//        CharSequence name = getString(R.string.app_name);// The user-visible name of the channel.
//        int importance = NotificationManager.IMPORTANCE_HIGH;
//        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//
//
//        Intent notificationIntent = new Intent(MyFirebaseMessagingService.this, MainActivity.class);
//            notificationIntent.putExtra("user_id",To_Id);
//            notificationIntent.putExtra("phone_no",To_Name);
//            notificationIntent.putExtra("user_name",From_Name);
//            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//
//        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
//            Notification notification = new NotificationCompat.Builder(this)
//                    .setCategory(Notification.CATEGORY_PROMO)
//                    .setContentTitle(getString(R.string.app_name))
//                    .setContentText("new message received")
//                    .setSmallIcon(R.mipmap.ic_launcher)
//                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
//                    .setAutoCancel(true)
//                    .setSound(uri)
//                    .setContentIntent(contentIntent)
//                    .setDefaults(Notification.DEFAULT_ALL)
//                    .setPriority(NotificationManager.IMPORTANCE_HIGH)
//                    .setPriority(NotificationManager.IMPORTANCE_MAX)
//                    .setPriority(Notification.PRIORITY_HIGH)
//                    .setChannelId(CHANNEL_ID)
//                    .setVibrate(new long[0]).build();
//            NotificationManager notificationManager =
//                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//            try {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                    notificationManager.createNotificationChannel(mChannel);
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            notificationManager.notify(0, notification);
//        }else{
//            Notification notification = new NotificationCompat.Builder(this)
//                    //.setContent(remoteViews)
//                    .setCategory(Notification.CATEGORY_PROMO)
//                    .setContentTitle(getString(R.string.app_name))
//                    .setContentText("new message received")
//                    .setSmallIcon(R.mipmap.ic_launcher)
//                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
//                    .setAutoCancel(true)
//                    .setSound(uri)
//                    .setContentIntent(contentIntent)
//                    .setDefaults(Notification.DEFAULT_ALL)
//                    .setPriority(NotificationManager.IMPORTANCE_HIGH)
//                    .setPriority(NotificationManager.IMPORTANCE_MAX)
//                    .setPriority(Notification.PRIORITY_HIGH)
//                    .setVibrate(new long[0]).build();
//
//
//
//            NotificationManager notificationManager =
//                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//            notificationManager.notify(0, notification);
//        }
//    }

    private void sendnotification(String to_Id,String emoji,String sound,String from_id,String name,String from_Name) {

        String Name = "";
//
        if (SharedPrefs.getSharedPref().getString(SharedPrefs.userdetail.user_id,"N/A").equalsIgnoreCase(from_id)){
            Name = name;
        }else {
            Name = from_Name;
        }

        Log.d("+++name","++++"+Name);

        Random random = new Random();
        int rand = random.nextInt(1000);
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(MyFirebaseMessagingService.this);
        builder.setContentTitle(Name);
        builder.setContentText("new message received");
        builder.setAutoCancel(true);
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setSound(uri);
        builder.setOnlyAlertOnce(true);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.mipmap.ic_launcher));
            builder.setColor(this.getResources().getColor(R.color.white));
        }

        Intent resultIntent = new Intent(this, MainActivity.class);
        resultIntent.putExtra("user_id",to_Id);
        resultIntent.putExtra("phone_no",name);
        resultIntent.putExtra("user_name",from_Name);
        resultIntent.putExtra("uid",from_id);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(0, builder.build());
    }


}