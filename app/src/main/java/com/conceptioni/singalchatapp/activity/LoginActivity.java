package com.conceptioni.singalchatapp.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.conceptioni.singalchatapp.utils.Constant;
import com.conceptioni.singalchatapp.R;
import com.conceptioni.singalchatapp.utils.SharedPrefs;
import com.conceptioni.singalchatapp.utils.Validations;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.ybs.countrypicker.CountryPicker;
import com.ybs.countrypicker.CountryPickerListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    EditText input_phone,input_password,input_country_code;
    AppCompatButton btn_login;
    TextView link_signup;
    CountryPicker picker;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initlogin();
        allclick();
    }

    private void allclick() {

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Validations validations = new Validations();
                if (!input_country_code.getText().toString().equalsIgnoreCase("")){
                    if (!input_phone.getText().toString().equalsIgnoreCase("")){
                        if (isValidPhoneNumber(input_phone.getText().toString())){
//                            boolean status = validateUsing_libphonenumber(input_country_code.getText().toString(), input_phone.getText().toString());
//                            Log.d("TAG", "onClick: " +status);
//                            if (status) {
                                if (!validations.isEmpty(input_password)) {
                                    if (validations.isValidPassword(input_password)) {
                                        Login();
                                    } else {
                                        input_password.requestFocus();
                                        input_password.setError("Please enter valid password");
                                    }
                                } else {
                                    input_password.requestFocus();
                                    input_password.setError("Please enter password");
                                }
//                            }else {
//                                input_phone.requestFocus();
//                                input_phone.setError("Enter Valid Phone number");
//                            }
                        }else {
                            input_phone.requestFocus();
                            input_phone.setError("Enter Valid Phone number");
                        }
                    }else {
                        input_phone.requestFocus();
                        input_phone.setError("Please enter phone number");
                    }
                }else {
                    input_country_code.requestFocus();
                    input_phone.setError("Please enter countrycode");
                }

            }
        });

        link_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this,SignUpActivity.class));
                finish();
            }
        });

    }

    private void initlogin() {
        input_phone = findViewById(R.id.input_phone);
        input_password = findViewById(R.id.input_password);
        btn_login = findViewById(R.id.btn_login);
        link_signup = findViewById(R.id.link_signup);
        input_country_code = findViewById(R.id.input_country_code);

        picker = CountryPicker.newInstance("Select Country");

        input_country_code.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View view) {
                openPicker();
                input_country_code.requestFocus();
                input_country_code.setShowSoftInputOnFocus(false);
            }
        });

        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String s, String s1, String s2, int i) {
                input_country_code.setText(s2);
                picker.dismiss();
            }
        });

    }

    public void openPicker(){
        picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
    }

    public boolean isPhoneNumberValid(String code,String phoneNumber)
    {
        //NOTE: This should probably be a member variable.
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        try
        {
            Phonenumber.PhoneNumber numberProto = phoneUtil.parse(phoneNumber, code);
            return phoneUtil.isValidNumber(numberProto);
        }
        catch (NumberParseException e)
        {
            System.err.println("NumberParseException was thrown: " + e.toString());
        }


        return false;
    }




    public void Login() {
        RequestQueue requestQueue = Volley.newRequestQueue(getApplication());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.Login, new Response.Listener<String>() {
            @Override
            public void onResponse(@Nullable String response) {
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        Log.d("+++login","+++ "+jsonObject);
                        if (jsonObject.getString("success").equalsIgnoreCase("1")){
                            Toast.makeText(LoginActivity.this,jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                            SharedPrefs.getSharedPref().edit().putString(SharedPrefs.userdetail.user_id, jsonObject.getString("user_id")).apply();
                            startActivity(new Intent(LoginActivity.this,HomeActivity.class));
                            finish();
                        }else {
                            Toast.makeText(LoginActivity.this,jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  new MakeToast("Could not able to login due to slow internet connectivity. Please try after some time");
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                HashMap<String, String> params = new HashMap<>();
                params.put("action", "login_user");
                params.put("var_phone", input_country_code.getText().toString() + input_phone.getText().toString());
                params.put("var_password", input_password.getText().toString());
                params.put("device_id", SharedPrefs.getSharedPref().getString(SharedPrefs.tokendetail.refreshtoken, "N/A"));

                Log.d("+++token", "++++" + params.toString() + "++++++" + SharedPrefs.getSharedPref().getString(SharedPrefs.tokendetail.refreshtoken, "N/A"));

                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private boolean validateUsing_libphonenumber(String countryCode, String phNumber) {
        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
        String isoCode = phoneNumberUtil.getRegionCodeForCountryCode(Integer.parseInt(countryCode));
        Log.d("TAG", "validateUsing_libphonenumber: " + isoCode);
        Phonenumber.PhoneNumber phoneNumber = null;
        try {
            //phoneNumber = phoneNumberUtil.parse(phNumber, "IN");  //if you want to pass region code
            phoneNumber = phoneNumberUtil.parse(phNumber, isoCode);
            Log.d("TAG", "validateUsing_libphonenumber: " +phoneNumber);
        } catch (NumberParseException e) {
            System.err.println(e);
        }

        boolean isValid = phoneNumberUtil.isValidNumber(phoneNumber);

        Log.d("TAG", "validateUsing_libphonenumber: " + phoneNumber + isValid);

        if (isValid) {
            String internationalFormat = phoneNumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.NATIONAL);
            Toast.makeText(this, "Phone Number is Valid " + internationalFormat, Toast.LENGTH_LONG).show();
            return true;
        } else {
            Toast.makeText(this, "Phone Number is Invalid " + phoneNumber, Toast.LENGTH_LONG).show();
            return false;
        }
    }

    private boolean isValidPhoneNumber(CharSequence phoneNumber) {
        if (!TextUtils.isEmpty(phoneNumber)) {
            return Patterns.PHONE.matcher(phoneNumber).matches();
        }
        return false;
    }
}
