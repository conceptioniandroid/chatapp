package com.conceptioni.singalchatapp.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.conceptioni.singalchatapp.R;
import com.conceptioni.singalchatapp.activity.MainActivity;
import com.conceptioni.singalchatapp.adapter.ChatAdapter;
import com.conceptioni.singalchatapp.model.ChatHistoryModel;
import com.conceptioni.singalchatapp.utils.Constant;
import com.conceptioni.singalchatapp.utils.RecyclerTouchListener;
import com.conceptioni.singalchatapp.utils.SharedPrefs;
import com.conceptioni.singalchatapp.utils.TextviewRegular;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ChatFragment extends Fragment {

    View view;
    ArrayList<ChatHistoryModel> chatHistoryModelArrayList = new ArrayList<>();
    RecyclerView chatrecycler;
    ChatAdapter chatAdapter;
    LinearLayout nodatall;
    boolean b = true;
    TextviewRegular nointernet;

    @SuppressLint("NewApi")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment1, container, false);
        initview();
        nointernet = view.findViewById(R.id.nointernet);
        if (b){
            if (isNetworkAvailable(Objects.requireNonNull(getActivity()))){
                getUserList();
            }else {
                nointernet.setVisibility(View.VISIBLE);
                chatrecycler.setVisibility(View.GONE);
            }
        }

        return view;

    }

    private void initview() {
        chatrecycler = view.findViewById(R.id.chatrecycler);
        nodatall = view.findViewById(R.id.nodatall);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        chatrecycler.setLayoutManager(linearLayoutManager);


        chatrecycler.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), chatrecycler, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {


                LinearLayout contactll = view.findViewById(R.id.contactll);
                contactll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String To_Id = "",To_Name = "",From_Name = "";
                        if (SharedPrefs.getSharedPref().getString(SharedPrefs.userdetail.user_id, "N/A").equalsIgnoreCase(chatHistoryModelArrayList.get(position).getFrom_id())) {
                            To_Id = chatHistoryModelArrayList.get(position).getTo_id();
                            From_Name = chatHistoryModelArrayList.get(position).getName();
                            To_Name = chatHistoryModelArrayList.get(position).getFrom_name();
                        } else {
                            To_Id = chatHistoryModelArrayList.get(position).getFrom_id();
                            From_Name = chatHistoryModelArrayList.get(position).getFrom_name();
                            To_Name = chatHistoryModelArrayList.get(position).getName();
                        }

//                        startActivity(new Intent(getActivity(), MainActivity.class).putExtra("user_id", To_Id).putExtra("phone_no", chatHistoryModelArrayList.get(position).getName()));
                        startActivity(new Intent(getActivity(),MainActivity.class).putExtra("user_id",To_Id).putExtra("phone_no",To_Name).putExtra("user_name", From_Name).putExtra("uid",SharedPrefs.getSharedPref().getString(SharedPrefs.userdetail.user_id,"N/A")));
                    }
                });
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

    }

    public void getUserList() {
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.Last_chat, new Response.Listener<String>() {
            @Override
            public void onResponse(@Nullable String response) {
                if (response != null) {
                    try {
                        chatHistoryModelArrayList.clear();
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("success").equalsIgnoreCase("1")) {
                            nodatall.setVisibility(View.GONE);
                            chatrecycler.setVisibility(View.VISIBLE);
                            nointernet.setVisibility(View.GONE);
                            JSONArray user_list = jsonObject.getJSONArray("last_chat");
                            for (int i = 0; i < user_list.length(); i++) {
                                JSONObject data = user_list.getJSONObject(i);
                                ChatHistoryModel chatHistoryModel = new ChatHistoryModel();
                                chatHistoryModel.setFrom_id(data.getString("from_id"));
                                chatHistoryModel.setTo_id(data.getString("to_id"));
                                chatHistoryModel.setInt_glcode(data.getString("int_glcode"));
                                chatHistoryModel.setVar_emoji(data.getString("var_emoji"));
                                chatHistoryModel.setVar_image(data.getString("var_image"));
                                chatHistoryModel.setVar_phone(data.getString("var_phone"));
                                chatHistoryModel.setName(data.getString("to_name"));
                                chatHistoryModel.setFrom_name(data.getString("from_name"));
                                chatHistoryModelArrayList.add(chatHistoryModel);
                            }
                            chatAdapter = new ChatAdapter(chatHistoryModelArrayList);
                            chatrecycler.setAdapter(chatAdapter);
                        } else {
                            chatrecycler.setVisibility(View.GONE);
                            nodatall.setVisibility(View.VISIBLE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  new MakeToast("Could not able to login due to slow internet connectivity. Please try after some time");
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                HashMap<String, String> params = new HashMap<>();
                params.put("action", "last_chat");
                params.put("from_id", SharedPrefs.getSharedPref().getString(SharedPrefs.userdetail.user_id, "N/A"));
                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @SuppressLint("NewApi")
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (getView() != null) {
            if (isNetworkAvailable(Objects.requireNonNull(getActivity()))){
                b = false;
                getUserList();
            }else {
                nointernet.setVisibility(View.VISIBLE);
                chatrecycler.setVisibility(View.GONE);

            }
        } else {
            b = true;
        }
    }

    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    Log.w("INTERNET:",String.valueOf(i));
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        Log.w("INTERNET:", "connected!");
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
