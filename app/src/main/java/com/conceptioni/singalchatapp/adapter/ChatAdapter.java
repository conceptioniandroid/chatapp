package com.conceptioni.singalchatapp.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.conceptioni.singalchatapp.R;
import com.conceptioni.singalchatapp.model.ChatHistoryModel;
import com.conceptioni.singalchatapp.utils.SharedPrefs;
import com.conceptioni.singalchatapp.utils.TextviewRegular;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import pl.droidsonroids.gif.GifImageView;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ChatViewHolder> {

    private Context context;
    ArrayList<ChatHistoryModel> chatHistoryModelArrayList;

    public ChatAdapter(ArrayList<ChatHistoryModel> chatHistoryModelArrayList ){
        this.chatHistoryModelArrayList = chatHistoryModelArrayList;
       
    }

    @NonNull
    @Override
    public ChatViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        context = viewGroup.getContext();
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;
        View view = layoutInflater.inflate(R.layout.row_item_chat, viewGroup, false);
        return new ChatViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatViewHolder ChatViewHolder, int i) {

        Glide.with(context) // replace with 'this' if it's in activity
                .load(chatHistoryModelArrayList.get(i).getVar_image())
                .into(ChatViewHolder.imageView);

        Log.d("+++++id","++++"+chatHistoryModelArrayList.get(i).getFrom_id() + "++++logi" + SharedPrefs.getSharedPref().getString(SharedPrefs.userdetail.user_id,"N/A") + "++++from" + chatHistoryModelArrayList.get(i).getFrom_name() + "+++to" + chatHistoryModelArrayList.get(i).getName());
//        ChatViewHolder.contactnotvr.setText(chatHistoryModelArrayList.get(i).getName());
        if (!SharedPrefs.getSharedPref().getString(SharedPrefs.userdetail.user_id,"N/A").equalsIgnoreCase(chatHistoryModelArrayList.get(i).getFrom_id())){
            ChatViewHolder.contactnotvr.setText(chatHistoryModelArrayList.get(i).getName());
        }else {
            if (chatHistoryModelArrayList.get(i).getFrom_name().equalsIgnoreCase("")){
                ChatViewHolder.contactnotvr.setText(chatHistoryModelArrayList.get(i).getVar_phone());
            }else {
                ChatViewHolder.contactnotvr.setText(chatHistoryModelArrayList.get(i).getFrom_name());
            }

        }


    }

    @Override
    public int getItemCount() {
        return chatHistoryModelArrayList.size();
    }

    public class ChatViewHolder extends RecyclerView.ViewHolder {
        TextviewRegular contactnotvr;
        LinearLayout contactll;
        ImageView imageView;
        public ChatViewHolder(@NonNull View itemView) {
            super(itemView);
            contactnotvr = itemView.findViewById(R.id.contactnotvr);
            contactll = itemView.findViewById(R.id.contactll);
            imageView = itemView.findViewById(R.id.lastmsgiv);
        }
    }

   
}
