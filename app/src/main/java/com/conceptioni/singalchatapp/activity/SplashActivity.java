package com.conceptioni.singalchatapp.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.conceptioni.singalchatapp.R;
import com.conceptioni.singalchatapp.adapter.ContactAdapter;
import com.conceptioni.singalchatapp.utils.SharedPrefs;

import java.util.Objects;

public class SplashActivity extends AppCompatActivity {

    public  static final int RequestPermissionCode  = 1 ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        init();
    }

    private void init() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                EnableRuntimePermission();
            }
        }, 5000);
    }

    @SuppressLint("NewApi")
    public void EnableRuntimePermission(){
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.READ_CONTACTS) ==
                    PackageManager.PERMISSION_GRANTED){
                if (!SharedPrefs.getSharedPref().getString(SharedPrefs.userdetail.user_id,"N/A").equalsIgnoreCase("N/A")){
                    startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                    finish();
                }else {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    finish();
                }
            }else {
                requestPermissions( new String[]{Manifest.permission.READ_CONTACTS},RequestPermissionCode);
            }
        }else {
            if (!SharedPrefs.getSharedPref().getString(SharedPrefs.userdetail.user_id,"N/A").equalsIgnoreCase("N/A")){
                startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                finish();
            }else {
                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                finish();
            }
        }


    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {
        switch (RC) {
            case RequestPermissionCode:
                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {
                    if (!SharedPrefs.getSharedPref().getString(SharedPrefs.userdetail.user_id,"N/A").equalsIgnoreCase("N/A")){
                        startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                        finish();
                    }else {
                        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                        finish();
                    }

                } else {
                    Toast.makeText(SplashActivity.this,"Permission Canceled, Now your application cannot access CONTACTS.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }
}
