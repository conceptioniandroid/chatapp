package com.conceptioni.singalchatapp.model;

public class ChatHistoryModel {

    String int_glcode;
    String dt_createddate;
    String var_emoji;
    String var_sound;
    String to_id;
    String var_phone;
    String name;
    String time;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getVar_image() {
        return var_image;
    }

    public void setVar_image(String var_image) {
        this.var_image = var_image;
    }

    String var_image;

    public String getFrom_name() {
        return from_name;
    }

    public void setFrom_name(String from_name) {
        this.from_name = from_name;
    }

    String from_name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ChatHistoryModel(){

    }

    public ChatHistoryModel(String id,String var_emoji,String var_sound,String var_image,String to_id,String from_id,String var_phone,String time){
        this.int_glcode = id;
        this.var_emoji = var_emoji;
        this.var_sound = var_sound;
        this.var_image = var_image;
        this.to_id = to_id;
        this.from_id = from_id;
        this.var_phone = var_phone;
        this.time = time;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    String datetime;

    public String getVar_phone() {
        return var_phone;
    }

    public void setVar_phone(String var_phone) {
        this.var_phone = var_phone;
    }

    public String getInt_glcode() {
        return int_glcode;
    }

    public void setInt_glcode(String int_glcode) {
        this.int_glcode = int_glcode;
    }

    public String getDt_createddate() {
        return dt_createddate;
    }

    public void setDt_createddate(String dt_createddate) {
        this.dt_createddate = dt_createddate;
    }

    public String getVar_emoji() {
        return var_emoji;
    }

    public void setVar_emoji(String var_emoji) {
        this.var_emoji = var_emoji;
    }

    public String getVar_sound() {
        return var_sound;
    }

    public void setVar_sound(String var_sound) {
        this.var_sound = var_sound;
    }

    public String getTo_id() {
        return to_id;
    }

    public void setTo_id(String to_id) {
        this.to_id = to_id;
    }

    public String getFrom_id() {
        return from_id;
    }

    public void setFrom_id(String from_id) {
        this.from_id = from_id;
    }

    String from_id;
}
