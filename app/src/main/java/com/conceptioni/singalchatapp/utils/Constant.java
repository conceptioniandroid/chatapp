package com.conceptioni.singalchatapp.utils;

import java.util.ArrayList;
import java.util.List;

public class Constant {

//    public static String ApiUrl = "http://cellix.co.il/smoji/apps/";
    public static String ApiUrl = "http://citechnology.in/poc/apps/";
    public static String Login = ApiUrl + "login.php";
    public static String Register = ApiUrl + "register.php";
    public static String UserList = ApiUrl + "user_list.php";
    public static String Last_chat = ApiUrl + "last_chat.php";
    public static String Media_list = ApiUrl + "media_list.php";
    public static String Add_chat = ApiUrl + "add_chat.php";
    public static String chat_history = ApiUrl + "chat_history.php";

    public static boolean active = false;
    public static String open_chat_id = "";
    public static String searchresult = "";

    public static List<String> notusingappcontactlist = new ArrayList<>();
    public static List<String> notusingappcontactnamelist = new ArrayList<>();

}
