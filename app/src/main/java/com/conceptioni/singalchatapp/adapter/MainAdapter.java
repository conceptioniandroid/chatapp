package com.conceptioni.singalchatapp.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.conceptioni.singalchatapp.model.ChatHistoryModel;
import com.conceptioni.singalchatapp.utils.Model;
import com.conceptioni.singalchatapp.R;
import com.conceptioni.singalchatapp.utils.SharedPrefs;
import com.conceptioni.singalchatapp.utils.TextviewRegular;

import java.util.ArrayList;

import pl.droidsonroids.gif.GifImageView;


public class MainAdapter extends RecyclerView.Adapter<MainAdapter.Holder> {
    private ArrayList<Model> matchesArrayList;
    ArrayList<ChatHistoryModel> chatHistoryModelArrayList;
    Context mContext;



    public MainAdapter(ArrayList<ChatHistoryModel> chatHistoryModelArrayList) {
        this.chatHistoryModelArrayList = chatHistoryModelArrayList;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return new Holder(layoutInflater.inflate(R.layout.row_layout, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull Holder holder, final int position) {

        Glide.with(mContext) // replace with 'this' if it's in activity
                .load(chatHistoryModelArrayList.get(position).getVar_image())
                .into(holder.gifImage);

        Glide.with(mContext) // replace with 'this' if it's in activity
                .load(chatHistoryModelArrayList.get(position).getVar_image())
                .into(holder.gifImageleft);


        if (SharedPrefs.getSharedPref().getString(SharedPrefs.userdetail.user_id,"N/A").equalsIgnoreCase(chatHistoryModelArrayList.get(position).getFrom_id())){
            holder.receivell.setVisibility(View.GONE);
            holder.sendll.setVisibility(View.VISIBLE);
        }else {
            holder.sendll.setVisibility(View.GONE);
            holder.receivell.setVisibility(View.VISIBLE);
        }

        holder.timetvr.setText(chatHistoryModelArrayList.get(position).getTime());
        holder.receivetvr.setText(chatHistoryModelArrayList.get(position).getTime());

//        if (matchesArrayList.get(position).getSubtitle().equals("1")) {
//            holder.gifImageView1.setVisibility(View.VISIBLE);
//            holder.gifImageView2.setVisibility(View.GONE);
//        } else {
//            holder.gifImageView1.setVisibility(View.GONE);
//            holder.gifImageView2.setVisibility(View.VISIBLE);
//        }

    }

    @Override
    public int getItemCount() {
        return chatHistoryModelArrayList.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        GifImageView gifImageView1, gifImageView2;
        ImageView gifImage,gifImageleft;
        LinearLayout sendll,receivell;
        TextviewRegular timetvr,receivetvr;

        Holder(@NonNull View itemView) {
            super(itemView);
            gifImageView1 = itemView.findViewById(R.id.gifImage1);
            gifImageView2 = itemView.findViewById(R.id.gifImage2);
            gifImage = itemView.findViewById(R.id.gifImage);
            gifImageleft = itemView.findViewById(R.id.gifImageleft);
            receivell = itemView.findViewById(R.id.receivell);
            sendll = itemView.findViewById(R.id.sendll);
            timetvr = itemView.findViewById(R.id.timetvr);
            receivetvr = itemView.findViewById(R.id.receivetvr);
        }
    }
}
