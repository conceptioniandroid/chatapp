package com.conceptioni.singalchatapp.model;

public class MediaListModel {

    String int_glcode;

    String var_image;

    String Var_image2;

    public String getVar_image2() {
        return Var_image2;
    }

    public void setVar_image2(String var_image2) {
        Var_image2 = var_image2;
    }

    public String getVar_image() {
        return var_image;
    }

    public void setVar_image(String var_image) {
        this.var_image = var_image;
    }

    public String getInt_glcode() {
        return int_glcode;
    }

    public void setInt_glcode(String int_glcode) {
        this.int_glcode = int_glcode;
    }

    public String getVar_emoji() {
        return var_emoji;
    }

    public void setVar_emoji(String var_emoji) {
        this.var_emoji = var_emoji;
    }

    public String getVar_sound() {
        return var_sound;
    }

    public void setVar_sound(String var_sound) {
        this.var_sound = var_sound;
    }

    String var_emoji;
    String var_sound;
}
