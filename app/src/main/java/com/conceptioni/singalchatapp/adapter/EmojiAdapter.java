package com.conceptioni.singalchatapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.conceptioni.singalchatapp.R;
import com.conceptioni.singalchatapp.model.MediaListModel;

import java.util.ArrayList;

public class EmojiAdapter extends RecyclerView.Adapter<EmojiAdapter.EmojiViewHolder> {
    private Context context;
    ArrayList<MediaListModel> mediaListModelArrayList = new ArrayList<>();

    public EmojiAdapter(ArrayList<MediaListModel> mediaListModelArrayList){
        this.mediaListModelArrayList = mediaListModelArrayList;
    }

    @NonNull
    @Override
    public EmojiViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        context = viewGroup.getContext();
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;
        View view = layoutInflater.inflate(R.layout.row_item_emoji, viewGroup, false);
        return new EmojiViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull EmojiViewHolder emojiViewHolder, int i) {

        Log.d("TAG", "onBindViewHolder: " +mediaListModelArrayList.get(i).getVar_image());

        Glide.with(context) // replace with 'this' if it's in activity
                .load(mediaListModelArrayList.get(i).getVar_image2())
                .into(emojiViewHolder.ivsmiley);
    }

    @Override
    public int getItemCount() {
        return mediaListModelArrayList.size();
    }

    public class EmojiViewHolder extends RecyclerView.ViewHolder {
        ImageView ivsmiley;
        LinearLayout linbtm;
        public EmojiViewHolder(@NonNull View itemView) {
            super(itemView);
            ivsmiley = itemView.findViewById(R.id.ivsmiley);
            linbtm = itemView.findViewById(R.id.linbtm);
        }
    }
}
