package com.conceptioni.singalchatapp.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.conceptioni.singalchatapp.activity.ChatApp;

public class SharedPrefs {

    public static SharedPreferences getSharedPref() {
        String sharedPrefenceName = "UserB";
        return ChatApp.getContext().getSharedPreferences(sharedPrefenceName, Context.MODE_PRIVATE);
    }

    public interface userdetail {
        String user_id = "user_id";
    }

    public interface tokendetail {
        String refreshtoken = "refreshtoken";
    }

}
