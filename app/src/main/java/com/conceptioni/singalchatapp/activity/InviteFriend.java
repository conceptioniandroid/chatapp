package com.conceptioni.singalchatapp.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.conceptioni.singalchatapp.R;
import com.conceptioni.singalchatapp.adapter.InviteAdapter;
import com.conceptioni.singalchatapp.utils.Constant;

public class InviteFriend extends AppCompatActivity {

    RecyclerView inviterecycler;
    ImageView backiv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.invitefriend);
        initinvite();
    }

    private void initinvite() {
        inviterecycler = findViewById(R.id.inviterecycler);
        backiv = findViewById(R.id.backiv);

        backiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(InviteFriend.this);
        inviterecycler.setLayoutManager(layoutManager);

        InviteAdapter adapter = new InviteAdapter(Constant.notusingappcontactlist,Constant.notusingappcontactnamelist);
        inviterecycler.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
