package com.conceptioni.singalchatapp.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.conceptioni.singalchatapp.R;
import com.conceptioni.singalchatapp.fragment.ChatFragment;
import com.conceptioni.singalchatapp.fragment.ContactFragment;


public class HomeActivity extends AppCompatActivity {


    TextView contactTextView;
    ImageView addiv;

    TabLayout tabLayout;
    private ViewPager viewpager;
    private int[] tabnames = {R.string.tab1,
            R.string.tab2
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        inithome();
    }

    private void inithome() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        viewpager = findViewById(R.id.viewpager);
        tabLayout = findViewById(R.id.tabs);
        addiv = findViewById(R.id.addiv);

        ViewPagerAdapter pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        if (viewpager != null) {
            viewpager.setAdapter(pagerAdapter);
        }
        if (tabLayout != null) {
            tabLayout.setupWithViewPager(viewpager);
            for (int i = 0; i < tabLayout.getTabCount(); i++) {
                TabLayout.Tab tab = tabLayout.getTabAt(i);
                if (tab != null)
                    tab.setCustomView(pagerAdapter.getTabView(i));
            }
            tabLayout.getTabAt(0).getCustomView().setSelected(true);
        }

        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                if (i == 0){
                    addiv.setVisibility(View.VISIBLE);
                    addiv.setImageResource(R.drawable.add_chat);
                }else if (i == 1){
                    addiv.setVisibility(View.GONE);
                    addiv.setImageResource(R.drawable.group);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        addiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               viewpager.setCurrentItem(1);
            }
        });

    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {

        final int PAGE_COUNT = 2;

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        View getTabView(int position) {
            @SuppressLint("InflateParams") View view = LayoutInflater.from(HomeActivity.this).inflate(R.layout.custom_home_tab, null);
            TextView tabnametvr = view.findViewById(R.id.title);
            tabnametvr.setText(tabnames[position]);

            return view;
        }

        @Nullable
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new ChatFragment();
                case 1:
                    return new ContactFragment();

            }
            return null;
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return super.getItemPosition(object);
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public void finishUpdate(ViewGroup container) {
            try {
                super.finishUpdate(container);
            } catch (NullPointerException nullPointerException) {
                System.out.println("Catch the NullPointerException in FragmentPagerAdapter.finishUpdate");
            }
        }
    }
}
