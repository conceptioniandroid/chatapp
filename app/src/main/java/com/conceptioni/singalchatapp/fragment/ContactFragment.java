package com.conceptioni.singalchatapp.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.conceptioni.singalchatapp.R;
import com.conceptioni.singalchatapp.activity.InviteFriend;
import com.conceptioni.singalchatapp.activity.MainActivity;
import com.conceptioni.singalchatapp.adapter.ContactAdapter;
import com.conceptioni.singalchatapp.model.ContactModel;
import com.conceptioni.singalchatapp.utils.Constant;
import com.conceptioni.singalchatapp.utils.RecyclerTouchListener;
import com.conceptioni.singalchatapp.utils.SharedPrefs;
import com.conceptioni.singalchatapp.utils.TextviewRegular;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class ContactFragment extends Fragment {

    public static final int RequestPermissionCode = 1;
    View view;
    RecyclerView contactrecycler;
    ArrayList<String> StoreContacts;

    ArrayList<String> tempStoreContacts = new ArrayList<>();
    ArrayList<String> tempStoreContactsName = new ArrayList<>();
    ArrayList<String> StoreContactsName = new ArrayList<>();
    ArrayList<String> ContactArrylist = new ArrayList<>();
    ArrayList<String> ContactIdArrylist = new ArrayList<>();
    ArrayList<String> ContactNameArrylist = new ArrayList<>();
    ArrayAdapter<String> arrayAdapter;
    Cursor cursor;
    String name, phonenumber,Type;
    ContactAdapter contactAdapter;
    ContactModel contactModel;
    ArrayList<ContactModel> contactModelArrayList = new ArrayList<>();
    List<String> NotUsingAppContactStringList = new ArrayList<>();
    String User_Name = "";
    android.support.v7.widget.SearchView searchView1;
    String id;
    ImageView invitefriendiv;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment2, container, false);
        initcontact();
        clicks();
        return view;
    }

    private void clicks() {

        searchView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchView1.setIconified(false);
            }
        });

        searchView1.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                StoreContacts.clear();
                StoreContactsName.clear();
                if (query.equalsIgnoreCase("")) {
                    StoreContacts.addAll(tempStoreContacts);
                    StoreContactsName.addAll(tempStoreContactsName);
                    contactAdapter.notifyDataSetChanged();
                } else {
                    for (int i = 0; i < tempStoreContactsName.size(); i++) {
                        Log.d("TAG", "onQueryTextChange: " +tempStoreContactsName.get(i)  + "++++" + query);
                        if (tempStoreContactsName.get(i).toUpperCase().contains(query.toUpperCase())) {
                            StoreContacts.add(tempStoreContacts.get(i));
                            StoreContactsName.add(tempStoreContactsName.get(i));
                        }
                    }
                    contactAdapter.notifyDataSetChanged();
                }
                //contactAdapter.getFilter().filter(query);
                return false;
            }
        });

        invitefriendiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(),InviteFriend.class));
            }
        });
    }

    private void initcontact() {
        searchView1 = view.findViewById(R.id.searchView1);
        contactrecycler = view.findViewById(R.id.contactrecycler);
        invitefriendiv = view.findViewById(R.id.invitefriendiv);
        StoreContacts = new ArrayList<String>();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        contactrecycler.setLayoutManager(linearLayoutManager);
//        EnableRuntimePermission();

        contactAdapter = new ContactAdapter(StoreContacts, ContactArrylist, contactModelArrayList, StoreContactsName);
        contactrecycler.setAdapter(contactAdapter);

        getUserList();

        contactrecycler.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), contactrecycler, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                final LinearLayout contactll = view.findViewById(R.id.contactll);
                final CircleImageView imageView = view.findViewById(R.id.image);
                final TextviewRegular contactnotvr = view.findViewById(R.id.contactnotvr);

                final String finalUser_Name = User_Name;
                contactll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        for (int i = 0; i < contactModelArrayList.size(); i++) {
                            if (StoreContacts.get(position).contains(ContactArrylist.get(i))) {
                                if (!SharedPrefs.getSharedPref().getString(SharedPrefs.userdetail.user_id, "N/A").equalsIgnoreCase(ContactIdArrylist.get(i))) {
                                    startActivity(new Intent(getActivity(), MainActivity.class).putExtra("user_id", ContactIdArrylist.get(i)).putExtra("phone_no", StoreContactsName.get(position)).putExtra("user_name", finalUser_Name).putExtra("uid", SharedPrefs.getSharedPref().getString(SharedPrefs.userdetail.user_id, "N/A")));
                                }
                            }
                        }
                    }
                });
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

    }


    @SuppressLint("NewApi")
    public void GetContactsIntoArrayList() {

//        String filter = ""+ ContactsContract.Contacts.HAS_PHONE_NUMBER + " > 0 and " + ContactsContract.CommonDataKinds.Phone.TYPE +"=" + ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE;
        cursor = Objects.requireNonNull(getActivity()).getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.Contacts.DISPLAY_NAME + " ASC ");

        if (cursor != null) {
            StoreContactsName.clear();
            while (cursor.moveToNext()) {
                id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                phonenumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                Type = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.ACCOUNT_TYPE_AND_DATA_SET));
                String contact_id = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
                phonenumber = phonenumber.replaceAll("\\s", "");


                Log.d("TAG", "GetContactsIntoArrayList: " + checkDuplicate(phonenumber.trim()));

                if (checkDuplicate(phonenumber.trim())) {
                    StoreContacts.add(phonenumber.trim());
                    tempStoreContacts.add(phonenumber.trim());
                    StoreContactsName.add(name);
                    tempStoreContactsName.add(name);
                }
            }

            Collections.sort(StoreContactsName);
            Collections.sort(tempStoreContactsName);
        }
        cursor.close();
    }

    private boolean checkDuplicate(String phoneno){
        for (int i = 0; i <StoreContacts.size() ; i++) {
            if (phoneno.equalsIgnoreCase(StoreContacts.get(i))){
                return false;
            }
        }
        return true;
    }

    public void getUserList() {
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.UserList, new Response.Listener<String>() {
            @Override
            public void onResponse(@Nullable String response) {
                if (response != null) {
                    try {
                        contactModelArrayList.clear();
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("success").equalsIgnoreCase("1")) {
                            JSONArray user_list = jsonObject.getJSONArray("user_list");
                            for (int i = 0; i < user_list.length(); i++) {
                                JSONObject data = user_list.getJSONObject(i);
                                ContactModel contactModel = new ContactModel();
                                contactModel.setId(data.optString("int_glcode"));
                                contactModel.setPhoneNo(data.optString("var_phone"));
                                contactModel.setName(data.optString("var_name"));
                                ContactArrylist.add(data.optString("var_phone"));
                                ContactIdArrylist.add(data.optString("int_glcode"));
                                ContactNameArrylist.add(data.optString("var_name"));
                                contactModelArrayList.add(contactModel);
                            }
                        }

                        GetContactsIntoArrayList();
                        contactAdapter.notifyDataSetChanged();

                        for (int i = 0; i < contactModelArrayList.size(); i++) {
                            if (SharedPrefs.getSharedPref().getString(SharedPrefs.userdetail.user_id, "N/A").equalsIgnoreCase(ContactIdArrylist.get(i))) {
                                for (int j = 0; j < StoreContactsName.size(); j++) {
                                    String No = contactModelArrayList.get(i).getPhoneNo();
                                    if (No.equalsIgnoreCase(StoreContacts.get(j)))
                                        User_Name = StoreContactsName.get(j);
                                }
                            }
                        }

                        Constant.notusingappcontactnamelist.clear();
                        Constant.notusingappcontactlist.clear();
                        for (int i = 0; i <StoreContacts.size() ; i++) {
                                boolean b = true;
                            for (int j = 0; j <contactModelArrayList.size() ; j++) {
                                if (contactModelArrayList.get(j).getPhoneNo().equalsIgnoreCase(StoreContacts.get(i))){
                                   b = false;
                                }
                            }
                            if (b){
                              Constant.notusingappcontactlist.add(StoreContacts.get(i));
                              Constant.notusingappcontactnamelist.add(StoreContactsName.get(i));
                            }
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  new MakeToast("Could not able to login due to slow internet connectivity. Please try after some time");
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                HashMap<String, String> params = new HashMap<>();
                params.put("action", "user_list");
                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }


}
