package com.conceptioni.singalchatapp.firebase;

import android.util.Log;

import com.conceptioni.singalchatapp.utils.SharedPrefs;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {


    @Override
    public void onTokenRefresh() {

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
//        final Intent intent = new Intent("tokenrefresh");
//        final LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(MyFirebaseInstanceIDService.this);
//        intent.putExtra("token_in _fcm_id",refreshedToken);
//        broadcastManager.sendBroadcast(intent);

        Log.d("++++++token","++++"+refreshedToken);
        SharedPrefs.getSharedPref().edit().putString(SharedPrefs.tokendetail.refreshtoken, refreshedToken).commit();

        Log.d("+++++token","++++"+SharedPrefs.getSharedPref().getString(SharedPrefs.tokendetail.refreshtoken, "N/A"));

    }




}