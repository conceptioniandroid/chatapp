package com.conceptioni.singalchatapp.utils;

import android.widget.EditText;

public class Validations {

    public boolean isEmpty(EditText editText) {
        return editText.getText().toString().equals("");
    }

    public boolean isValidPhoneNumber(EditText phoneNumber) {
        return phoneNumber.getText().toString().length() == 10;
    }

    public boolean isValidPassword(EditText password) {
        if (password.getText().toString().length() >= 6) return true;
        else return false;
    }
}
