package com.conceptioni.singalchatapp.activity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.conceptioni.singalchatapp.R;
import com.conceptioni.singalchatapp.adapter.EmojiAdapter;
import com.conceptioni.singalchatapp.adapter.MainAdapter;
import com.conceptioni.singalchatapp.model.ChatHistoryModel;
import com.conceptioni.singalchatapp.model.MediaListModel;
import com.conceptioni.singalchatapp.utils.Constant;
import com.conceptioni.singalchatapp.utils.GlideDrawableImageViewTarget;
import com.conceptioni.singalchatapp.utils.Model;
import com.conceptioni.singalchatapp.utils.RecyclerTouchListener;
import com.conceptioni.singalchatapp.utils.SharedPrefs;
import com.conceptioni.singalchatapp.utils.TextviewRegular;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import pl.droidsonroids.gif.GifImageView;

public class MainActivity extends AppCompatActivity {

    RecyclerView rec_list;
    MainAdapter mAdapter1;
    MediaPlayer mp;
    TextView tv;
    int i = 0;
    ArrayList<Model> modelArrayList = new ArrayList<>();
    String Chat_Id, Media_Id, phone_no = "", User_Name = "", From_Id = "";
    RecyclerView emojirecylcer;
    ArrayList<MediaListModel> mediaListModelArrayList = new ArrayList<>();
    ArrayList<ChatHistoryModel> chatHistoryModelArrayList = new ArrayList<>();
    EmojiAdapter emojiAdapter;
    TextviewRegular toolbar;
    ImageView chativ;

    private BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LoadPreviousChat();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupData();
        clicks();
//        setupTimer();

    }

    private void setupData() {
        rec_list = findViewById(R.id.rec);
        emojirecylcer = findViewById(R.id.emojirecylcer);
        chativ = findViewById(R.id.chativ);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rec_list.setLayoutManager(linearLayoutManager);
        rec_list.setItemAnimator(new DefaultItemAnimator());
        linearLayoutManager.setStackFromEnd(true);

        toolbar = findViewById(R.id.toolbar);
        if (getIntent().getExtras() != null) {
            Chat_Id = getIntent().getStringExtra("user_id");
            phone_no = getIntent().getStringExtra("phone_no");
            User_Name = getIntent().getStringExtra("user_name");
            From_Id = getIntent().getStringExtra("uid");
            Log.d("++phone", "+++++" + phone_no + "++++id" + Chat_Id + "+++++" + User_Name + "From_Id" + From_Id);

            if (phone_no.equalsIgnoreCase("")) {
                toolbar.setText("Chat");
            } else {
                if (!SharedPrefs.getSharedPref().getString(SharedPrefs.userdetail.user_id, "N/A").equalsIgnoreCase(From_Id)) {
                    toolbar.setText(User_Name);
                } else {
                    toolbar.setText(phone_no);
                }

//                toolbar.setText(phone_no);
            }

        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, false);
        emojirecylcer.setLayoutManager(layoutManager);
        emojirecylcer.setItemAnimator(new DefaultItemAnimator());

        getMediaList();

        LoadPreviousChat();

        emojirecylcer.addOnItemTouchListener(new RecyclerTouchListener(MainActivity.this, emojirecylcer, new RecyclerTouchListener.ClickListener() {
            @SuppressLint("SimpleDateFormat")
            @Override
            public void onClick(View view, int position) {
                Media_Id = mediaListModelArrayList.get(position).getInt_glcode();
//                Date currentTime = Calendar.getInstance().getTime();
                SimpleDateFormat currentTime = new SimpleDateFormat("dd:MMM hh:mm a");
                String time = currentTime.format(new Date());
//                new SimpleDateFormat("hh:mm a").format(new Date());
                System.out.println(time);
                SendChatMsg(mediaListModelArrayList.get(position).getVar_emoji(), mediaListModelArrayList.get(position).getVar_sound(), mediaListModelArrayList.get(position).getVar_image(),time);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        rec_list.addOnItemTouchListener(new RecyclerTouchListener(MainActivity.this, rec_list, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                LinearLayout send = view.findViewById(R.id.sendll);
                LinearLayout receive = view.findViewById(R.id.receivell);
                final GifImageView gifImageView = view.findViewById(R.id.gifImage1);
                final ImageView gifImage = view.findViewById(R.id.gifImage);
                final ImageView gifImageleft = view.findViewById(R.id.gifImageleft);
                final GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(gifImage, 1);
                final GlideDrawableImageViewTarget imageViewTarget2 = new GlideDrawableImageViewTarget(gifImageleft, 1);
                send.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Glide.with(MainActivity.this) // replace with 'this' if it's in activity
                                .load(chatHistoryModelArrayList.get(position).getVar_emoji())
                                .into(imageViewTarget);

                        if (mp != null && mp.isPlaying()) {
                            mp.stop();
                        }

                        mp = MediaPlayer.create(getApplicationContext(), Uri.parse(chatHistoryModelArrayList.get(position).getVar_sound()));
                        mp.start();

                        // wait 5 sec... then stop the player.
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (mp.isPlaying())
                                mp.stop();
                                Glide.with(getApplicationContext()) // replace with 'this' if it's in activity
                                        .load(chatHistoryModelArrayList.get(position).getVar_image())
                                        .into(gifImage);
                            }
                        }, 12000);//millisec.


                    }
                });

                receive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Glide.with(MainActivity.this) // replace with 'this' if it's in activity
                                .load(chatHistoryModelArrayList.get(position).getVar_emoji())
                                .into(imageViewTarget2);

                        if (mp != null && mp.isPlaying()) {
                            mp.stop();
                        }

                        mp = MediaPlayer.create(getApplicationContext(), Uri.parse(chatHistoryModelArrayList.get(position).getVar_sound()));
                        mp.start();

                        // wait 5 sec... then stop the player.
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mp.stop();
                                Glide.with(MainActivity.this) // replace with 'this' if it's in activity
                                        .load(chatHistoryModelArrayList.get(position).getVar_image())
                                        .into(gifImageleft);
                            }
                        }, 12000);//millisec.


                    }
                });
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void clicks() {

        chativ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        findViewById(R.id.ivsmiley).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modelArrayList.add(new Model("MainTitle1", "1"));
                mAdapter1.notifyDataSetChanged();
                rec_list.scrollToPosition(modelArrayList.size() - 1);

                if (mp != null && mp.isPlaying()) {
                    mp.stop();
                }

                mp = MediaPlayer.create(getApplicationContext(), R.raw.one);
                mp.start();

            }
        });

        findViewById(R.id.ivclap).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modelArrayList.add(new Model("MainTitle1", "2"));
                mAdapter1.notifyDataSetChanged();
                rec_list.scrollToPosition(modelArrayList.size() - 1);

                if (mp != null && mp.isPlaying()) {
                    mp.stop();
                }
                mp = MediaPlayer.create(getApplicationContext(), R.raw.two);
                mp.start();

            }
        });
    }

    private void setupTimer() {
        Timer T = new Timer();
        T.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        i++;

                        if (i == 1200) {
                            closeapp();
                        }
                    }
                });
            }
        }, 1000, 6000000);
    }

    @SuppressLint("SetTextI18n")
    private void closeapp() {
        tv.setText("he llo");
    }

    public void getMediaList() {
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.Media_list, new Response.Listener<String>() {
            @Override
            public void onResponse(@Nullable String response) {
                if (response != null) {
                    try {
                        mediaListModelArrayList.clear();
                        JSONObject jsonObject = new JSONObject(response);

                        Log.d("TAG", "onResponse: " + jsonObject.toString());

                        if (jsonObject.getString("success").equalsIgnoreCase("1")) {
                            JSONArray user_list = jsonObject.getJSONArray("media_list");
                            Log.d("TAG", "onResponse: " + user_list.length());
                            for (int i = 0; i < user_list.length(); i++) {
                                JSONObject data = user_list.getJSONObject(i);
                                MediaListModel mediaListModel = new MediaListModel();
                                mediaListModel.setInt_glcode(data.optString("int_glcode"));
                                mediaListModel.setVar_emoji(data.optString("var_emoji"));
                                mediaListModel.setVar_sound(data.optString("var_sound"));
                                mediaListModel.setVar_image(data.getString("var_image"));
                                mediaListModel.setVar_image2(data.getString("chat_image"));
                                Log.d("TAG", "onResponse: " + data.getString("var_image"));
                                mediaListModelArrayList.add(mediaListModel);
                            }
                            emojiAdapter = new EmojiAdapter(mediaListModelArrayList);
                            emojirecylcer.setAdapter(emojiAdapter);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  new MakeToast("Could not able to login due to slow internet connectivity. Please try after some time");
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                HashMap<String, String> params = new HashMap<>();
                params.put("action", "media_list");
                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void SendChatMsg(final String emoji, final String sound, final String image, final String time) {
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.Add_chat, new Response.Listener<String>() {
            @Override
            public void onResponse(@Nullable String response) {
                if (response != null) {
                    try {
//                        mediaListModelArrayList.clear();
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("success").equalsIgnoreCase("1")) {

                            Log.d("++++++size", "+++++" + chatHistoryModelArrayList.size());

                            if (!chatHistoryModelArrayList.isEmpty()) {
                                chatHistoryModelArrayList.add(new ChatHistoryModel("", emoji, sound, image, Chat_Id, SharedPrefs.getSharedPref().getString(SharedPrefs.userdetail.user_id, "N/A"), phone_no,time));
                                mAdapter1.notifyDataSetChanged();
                                rec_list.scrollToPosition(chatHistoryModelArrayList.size() - 1);
                            } else {
                                ChatHistoryModel chatHistoryModel = new ChatHistoryModel();
                                chatHistoryModel.setInt_glcode("");
                                chatHistoryModel.setVar_emoji(emoji);
                                chatHistoryModel.setVar_sound(sound);
                                chatHistoryModel.setVar_image(image);
                                chatHistoryModel.setTo_id(Chat_Id);
                                chatHistoryModel.setFrom_id(SharedPrefs.getSharedPref().getString(SharedPrefs.userdetail.user_id, "N/A"));
                                chatHistoryModel.setVar_phone(phone_no);
                                chatHistoryModel.setDatetime("");
                                chatHistoryModel.setTime(time);
                                chatHistoryModelArrayList.add(chatHistoryModel);

                                if (!chatHistoryModelArrayList.isEmpty()) {
                                    mAdapter1 = new MainAdapter(chatHistoryModelArrayList);
                                    rec_list.setAdapter(mAdapter1);
                                }

                            }



                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  new MakeToast("Could not able to login due to slow internet connectivity. Please try after some time");
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<>();
                params.put("action", "add_chat");
                params.put("media_id", Media_Id);
                params.put("to_id", Chat_Id);
                params.put("from_id", SharedPrefs.getSharedPref().getString(SharedPrefs.userdetail.user_id, "N/A"));
                params.put("to_name", phone_no);
                params.put("from_name", User_Name);

                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void LoadPreviousChat() {
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.chat_history, new Response.Listener<String>() {
            @Override
            public void onResponse(@Nullable String response) {
                if (response != null) {
                    try {
                        chatHistoryModelArrayList.clear();
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("success").equalsIgnoreCase("1")) {
                            JSONArray user_list = jsonObject.getJSONArray("chat_history");
                            for (int i = 0; i < user_list.length(); i++) {
                                JSONObject data = user_list.getJSONObject(i);
                                ChatHistoryModel chatHistoryModel = new ChatHistoryModel();
                                chatHistoryModel.setInt_glcode(data.optString("int_glcode"));
                                chatHistoryModel.setVar_emoji(data.optString("var_emoji"));
                                chatHistoryModel.setVar_sound(data.optString("var_sound"));
                                chatHistoryModel.setTo_id(data.optString("to_id"));
                                chatHistoryModel.setFrom_id(data.optString("from_id"));
                                chatHistoryModel.setDatetime(data.optString("datetime"));
                                chatHistoryModel.setVar_image(data.optString("var_image"));
                                chatHistoryModel.setTime(data.optString("time"));
                                chatHistoryModelArrayList.add(chatHistoryModel);
                            }

                            Log.d("+++++++size", "+++++" + chatHistoryModelArrayList.size());
                            mAdapter1 = new MainAdapter(chatHistoryModelArrayList);
                            rec_list.setAdapter(mAdapter1);


                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  new MakeToast("Could not able to login due to slow internet connectivity. Please try after some time");
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                if (SharedPrefs.getSharedPref().getString(SharedPrefs.userdetail.user_id, "N/A").equalsIgnoreCase(Chat_Id)) {
                    Chat_Id = From_Id;
                } else {
                    Chat_Id = Chat_Id;
                }

                HashMap<String, String> params = new HashMap<>();
                params.put("action", "chat_history");
                params.put("to_id", Chat_Id);
                params.put("from_id", SharedPrefs.getSharedPref().getString(SharedPrefs.userdetail.user_id, "N/A"));

                Log.d("+++++param", "++++++" + params.toString());

                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.active = true;
        Constant.open_chat_id = Chat_Id;
        registerReceiver(messageReceiver, new IntentFilter("broadcast_chat_message"));
    }

    @Override
    public void onStart() {
        super.onStart();
        /*for update ui*/
        Constant.active = true;
        Constant.open_chat_id = Chat_Id;
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mp != null) {
            mp.stop();
//            mp.release();
        }
        /*for update ui*/
        Constant.active = false;
        Constant.open_chat_id = "";
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mp != null) {
           mp.stop();
        }

        /*for update ui*/
        LocalBroadcastManager.getInstance(this).unregisterReceiver(messageReceiver);
        super.onPause();
        Constant.active = false;
        Constant.open_chat_id = "";
    }
}
