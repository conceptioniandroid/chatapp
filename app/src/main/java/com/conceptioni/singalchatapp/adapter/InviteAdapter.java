package com.conceptioni.singalchatapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.conceptioni.singalchatapp.R;
import com.conceptioni.singalchatapp.model.ContactModel;
import com.conceptioni.singalchatapp.utils.TextviewRegular;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class InviteAdapter extends RecyclerView.Adapter<InviteAdapter.ContactViewHolder> {

    private Context context;
    private List<String> StoreContacts;
    private List<String> StoreContactsName;

    //CustomFilter filter;
    public InviteAdapter(List<String> StoreContacts, List<String> StoreContactsName) {
        this.StoreContacts = StoreContacts;
        this.StoreContactsName = StoreContactsName;
        //this.filterList=StoreContactsName;
    }

    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        context = viewGroup.getContext();
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;
        View view = layoutInflater.inflate(R.layout.row_item_invite_contact, viewGroup, false);
        return new ContactViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactViewHolder contactViewHolder, int i) {
        contactViewHolder.contactnotvr.setText(StoreContacts.get(i));
        contactViewHolder.contactnametvr.setText(StoreContactsName.get(i));
    }

    @Override
    public int getItemCount() {
        return StoreContacts.size();
    }



    public class ContactViewHolder extends RecyclerView.ViewHolder {
        TextviewRegular contactnotvr, contactnametvr;
        LinearLayout contactll;
        CircleImageView image;
        View view;

        public ContactViewHolder(@NonNull View itemView) {
            super(itemView);
            contactnotvr = itemView.findViewById(R.id.contactnotvr);
            contactnametvr = itemView.findViewById(R.id.contactnametvr);
            contactll = itemView.findViewById(R.id.contactll);
            image = itemView.findViewById(R.id.image);
            view = itemView.findViewById(R.id.view);
        }
    }

}
