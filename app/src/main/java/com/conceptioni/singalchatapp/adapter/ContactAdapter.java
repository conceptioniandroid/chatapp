package com.conceptioni.singalchatapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;

import com.conceptioni.singalchatapp.R;
import com.conceptioni.singalchatapp.activity.ChatApp;
import com.conceptioni.singalchatapp.model.ContactModel;
import com.conceptioni.singalchatapp.utils.TextviewRegular;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactViewHolder> {

    private Context context;
    private ArrayList<String> StoreContacts;
    private ArrayList<String> StoreContactsName;
    private ArrayList<String> ContactArrayList;
    ArrayList<ContactModel> contactModelArrayList = new ArrayList<>();
    ArrayList<String> filterList;
    //CustomFilter filter;
    public ContactAdapter(ArrayList<String> StoreContacts, ArrayList<String> ContactArrayList, ArrayList<ContactModel> contactModelArrayList, ArrayList<String> StoreContactsName) {
        this.StoreContacts = StoreContacts;
        this.ContactArrayList = ContactArrayList;
        this.contactModelArrayList = contactModelArrayList;
        this.StoreContactsName = StoreContactsName;
        //this.filterList=StoreContactsName;
    }

    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        context = viewGroup.getContext();
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;
        View view = layoutInflater.inflate(R.layout.row_item_contact, viewGroup, false);
        return new ContactViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactViewHolder contactViewHolder, int i) {
        contactViewHolder.contactnotvr.setText(StoreContacts.get(i));
        contactViewHolder.contactnametvr.setText(StoreContactsName.get(i));

        Log.d("TAG", "onBindViewHolder: " + StoreContacts.get(i));

        if (ContactArrayList.contains(StoreContacts.get(i).trim())) {
            contactViewHolder.contactll.setVisibility(View.VISIBLE);
            contactViewHolder.view.setVisibility(View.VISIBLE);
            contactViewHolder.image.setAlpha((int) 1.0);
            contactViewHolder.contactnotvr.setTextColor(context.getResources().getColor(R.color.black));
            contactViewHolder.contactnametvr.setTextColor(context.getResources().getColor(R.color.black));
        } else {
            contactViewHolder.contactll.setVisibility(View.GONE);
            contactViewHolder.view.setVisibility(View.GONE);
            contactViewHolder.image.setAlpha((int) 0.5);
            contactViewHolder.contactnotvr.setTextColor(context.getResources().getColor(R.color.blackopacity));
            contactViewHolder.contactnametvr.setTextColor(context.getResources().getColor(R.color.blackopacity));
        }
        
//        if (ContactArrayList.size() == ContactArrayList.size() - 1){
//            contactViewHolder.view.setVisibility(View.GONE);
//        }else {
//            contactViewHolder.view.setVisibility(View.VISIBLE);
//        }
    }

    @Override
    public int getItemCount() {
        return StoreContacts.size();
    }



    public class ContactViewHolder extends RecyclerView.ViewHolder {
        TextviewRegular contactnotvr, contactnametvr;
        LinearLayout contactll;
        CircleImageView image;
        View view;

        public ContactViewHolder(@NonNull View itemView) {
            super(itemView);
            contactnotvr = itemView.findViewById(R.id.contactnotvr);
            contactnametvr = itemView.findViewById(R.id.contactnametvr);
            contactll = itemView.findViewById(R.id.contactll);
            image = itemView.findViewById(R.id.image);
            view = itemView.findViewById(R.id.view);
        }
    }

}
